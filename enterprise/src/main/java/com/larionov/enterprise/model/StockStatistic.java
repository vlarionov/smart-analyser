package com.larionov.enterprise.model;

import com.larionov.enterprise.model.validation.OrderedList;
import com.sun.istack.internal.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

public class StockStatistic extends BaseEntity {
    @NotNull
    private final Stock stock;
    @NotNull
    @OrderedList
    private List<Price> priceList;

    public StockStatistic(Stock stock, List<Price> priceList) {
        this.stock = stock;
        this.priceList = priceList;
    }

    public StockStatistic(Stock stock, List<Price> priceList, long id) {
        super(id);
        this.stock = stock;
        this.priceList = priceList;
    }

    public Stock getStock() {
        return stock;
    }

    public List<Price> getPriceList() {
        return priceList;
    }

}
