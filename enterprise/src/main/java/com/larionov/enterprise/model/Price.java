package com.larionov.enterprise.model;

import java.time.Instant;

public class Price extends InstantEntity {
    private final double price;

    public Price(double price) {
        super();
        this.price = price;
    }

    public Price(double price, Instant instant) {
        super(instant);
        this.price = price;
    }

    public Price(double price, Instant instant, long id) {
        super(instant, id);
        this.price = price;
    }


    public double getPrice() {
        return price;
    }

}
