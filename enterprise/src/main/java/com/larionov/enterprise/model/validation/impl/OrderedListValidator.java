package com.larionov.enterprise.model.validation.impl;

import com.larionov.enterprise.model.InstantEntity;
import com.larionov.enterprise.model.validation.OrderedList;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.security.InvalidParameterException;
import java.util.Iterator;
import java.util.List;

public class OrderedListValidator implements ConstraintValidator<OrderedList, Object> {
    @Override
    public void initialize(OrderedList constraintAnnotation) {

    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) return true;

        try {
            List<?> list = (List) value;
            Iterator<?> iterator = list.iterator();
            InstantEntity previous = (InstantEntity) iterator.next();
            if (!list.isEmpty()) {
                do {
                    InstantEntity current = (InstantEntity) iterator.next();
                    if (previous.compareTo(current) > 0) {
                        throw new InvalidParameterException();
                    }
                } while (iterator.hasNext());
            }
            return true;
        } catch (ClassCastException | InvalidParameterException e) {
            context.buildConstraintViolationWithTemplate("Not a ordered list");
            return false;
        }
    }

}
