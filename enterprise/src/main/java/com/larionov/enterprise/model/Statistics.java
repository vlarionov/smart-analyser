package com.larionov.enterprise.model;

import java.util.Collections;
import java.util.List;

public class Statistics<I extends InstantEntity> {

    private final List<I> instantList;

    public Statistics(List<I> instantList) {
        this.instantList = Collections.unmodifiableList(instantList);
    }

    public List<I> getInstantList() {
        return instantList;
    }
}
