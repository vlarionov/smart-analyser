package com.larionov.enterprise.model.validation;

import com.larionov.enterprise.model.validation.impl.OrderedListValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.util.List;

@Documented
@Target( { ElementType.FIELD } )
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = OrderedListValidator.class)
public @interface OrderedList {
    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}