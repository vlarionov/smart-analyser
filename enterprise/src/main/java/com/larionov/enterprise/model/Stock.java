package com.larionov.enterprise.model;


import com.larionov.enterprise.model.BaseEntity;
import com.sun.org.glassfish.external.statistics.Statistic;

import javax.validation.constraints.NotNull;
import java.util.List;

public class Stock extends BaseEntity {
    @NotNull
    private final String companyName;

    public Stock(String companyName) {
        this.companyName = companyName;
    }

    public Stock(long id, String companyName) {
        super(id);
        this.companyName = companyName;
    }

    public String getCompanyName() {
        return companyName;
    }

}
