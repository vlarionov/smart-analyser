package com.larionov.enterprise.model;

import java.util.List;

public class AnalysisResult extends BaseEntity {
    //TODO make ordered constraint
    private final List<Portfolio> portfolioList;

    public AnalysisResult(List<Portfolio> portfolioList) {
        this.portfolioList = portfolioList;
    }

    public AnalysisResult(List<Portfolio> portfolioList, long id) {
        super(id);
        this.portfolioList = portfolioList;
    }

    public List<Portfolio> getPortfolioList() {
        return portfolioList;
    }
}
