package com.larionov.enterprise.model;

import java.time.Instant;

public class Portfolio extends InstantEntity {
    private final double balance;
    private final double stockAmount;

    public Portfolio(double balance, double stockAmount, Instant instant) {
        super(instant);
        this.balance = balance;
        this.stockAmount = stockAmount;
    }

    public Portfolio(double balance, double stockAmount, Instant instant, long id) {
        super(instant, id);
        this.balance = balance;
        this.stockAmount = stockAmount;
    }

    public double getBalance() {
        return balance;
    }

    public double getStockAmount() {
        return stockAmount;
    }
}
