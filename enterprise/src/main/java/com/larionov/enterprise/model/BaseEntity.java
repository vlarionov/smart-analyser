package com.larionov.enterprise.model;

import javax.validation.constraints.Min;

public class BaseEntity {
    @Min(0)
    private final long id;

    public BaseEntity() {
        id = 0;
    }

    public BaseEntity(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseEntity)) return false;

        BaseEntity that = (BaseEntity) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
