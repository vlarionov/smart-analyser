package com.larionov.enterprise.model;

import javax.validation.constraints.NotNull;
import java.time.Instant;

public class InstantEntity extends BaseEntity implements Comparable<InstantEntity> {
    @NotNull
    private final Instant instant;

    public InstantEntity() {
        instant = Instant.MIN;
    }

    public InstantEntity(Instant instant) {
        this.instant = instant;
    }

    public InstantEntity(Instant instant, long id) {
        super(id);
        this.instant = instant;
    }

    public Instant getInstant() {
        return instant;
    }

    @Override
    public int compareTo(@NotNull InstantEntity otherInstantEntity) {
        return instant.compareTo(otherInstantEntity.getInstant());
    }

}
