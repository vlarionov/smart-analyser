package com.larionov.enterprise.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
        "com.larionov.enterprise.dao",
        "com.larionov.enterprise.service"
})
public class DataConfig {
}
