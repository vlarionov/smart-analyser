package com.larionov.enterprise.dao;

import com.larionov.enterprise.model.StockStatistic;

import java.util.List;

public interface StockStatisticsDao extends CrudDao<StockStatistic> {
    @Override
    List<StockStatistic> findAll();
}
