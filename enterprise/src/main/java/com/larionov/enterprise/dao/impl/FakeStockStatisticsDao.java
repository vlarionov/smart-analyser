package com.larionov.enterprise.dao.impl;

import com.larionov.enterprise.model.Price;
import com.larionov.enterprise.model.Stock;
import com.larionov.enterprise.model.StockStatistic;
import com.larionov.enterprise.dao.StockStatisticsDao;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Repository
public class FakeStockStatisticsDao implements StockStatisticsDao {
    private final StockStatistic stockStatistic;


    public FakeStockStatisticsDao() {
        stockStatistic = createStockStatistic();
    }

    private Stock createStock() {
        return new Stock(0, "ExampleCompany");
    }

    private StockStatistic createStockStatistic() {
        return new StockStatistic(createStock(), getPriceQuoteList());
    }

    public List<Price> getPriceQuoteList() {
        Instant now = Instant.now();
        return Arrays.asList(
                new Price(0, now),
                new Price(1, now.plus(1, ChronoUnit.SECONDS)),
                new Price(2, now.plus(2, ChronoUnit.SECONDS)),
                new Price(1, now.plus(3, ChronoUnit.SECONDS)),
                new Price(2, now.plus(4, ChronoUnit.SECONDS)),
                new Price(3, now.plus(5, ChronoUnit.SECONDS)),
                new Price(2, now.plus(6, ChronoUnit.SECONDS)),
                new Price(1, now.plus(7, ChronoUnit.SECONDS)),
                new Price(0, now.plus(8, ChronoUnit.SECONDS))
        );
    }

    @Override
    public List<StockStatistic> findAll() {
        return Collections.singletonList(stockStatistic);
    }
}
