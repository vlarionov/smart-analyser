package com.larionov.enterprise.service;

import com.larionov.enterprise.model.StockStatistic;
import com.larionov.enterprise.dao.StockStatisticsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockStatisticsService {
    private final StockStatisticsDao statisticsProvider;

    @Autowired
    public StockStatisticsService(StockStatisticsDao statisticsProvider) {
        this.statisticsProvider = statisticsProvider;
    }

    public StockStatistic get() {
        return statisticsProvider.get();
    }
}

