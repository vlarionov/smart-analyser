package com.larionov.enterprise.service;

import com.larionov.enterprise.model.AnalysisResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnalysisService {
    private final StockStatisticsService stockStatisticsService;

    @Autowired
    public AnalysisService(StockStatisticsService stockStatisticsService) {
        this.stockStatisticsService = stockStatisticsService;
    }

    public AnalysisResult getAnalysisResult() {
        return null;
    }
}
