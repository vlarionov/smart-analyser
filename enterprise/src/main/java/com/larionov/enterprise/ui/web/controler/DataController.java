package com.larionov.enterprise.ui.web.controler;

import com.larionov.enterprise.service.StockStatisticsService;
import com.larionov.enterprise.ui.web.controler.dto.StockStatisticDto;
import com.larionov.enterprise.ui.web.controler.dto.converter.StockStatisticConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("data")
public class DataController {
    private final StockStatisticsService stockStatisticsService;
    private final StockStatisticConverter stockStatisticConverter;

    @Autowired
    public DataController(
            StockStatisticsService stockStatisticsService,
            StockStatisticConverter stockStatisticConverter
    ) {
        this.stockStatisticsService = stockStatisticsService;
        this.stockStatisticConverter = stockStatisticConverter;
    }

    @RequestMapping(value = "get", method = {RequestMethod.GET})
    @ResponseBody
    public StockStatisticDto onGet() {
        return stockStatisticConverter.convert(stockStatisticsService.get());
    }
}
