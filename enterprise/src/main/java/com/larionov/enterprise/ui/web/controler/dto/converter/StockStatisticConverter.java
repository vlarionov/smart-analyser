package com.larionov.enterprise.ui.web.controler.dto.converter;

import com.larionov.enterprise.model.StockStatistic;
import com.larionov.enterprise.ui.web.controler.dto.StockStatisticDto;
import org.springframework.stereotype.Component;

@Component
public class StockStatisticConverter  {
    public StockStatisticDto convert(StockStatistic stockStatistic) {
        return new StockStatisticDto(stockStatistic);
    }
}
