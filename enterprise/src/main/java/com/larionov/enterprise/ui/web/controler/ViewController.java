package com.larionov.enterprise.ui.web.controler;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class ViewController {
    @RequestMapping(value = "/", method = {RequestMethod.GET})
    String index() {
        return "index";
    }
}
