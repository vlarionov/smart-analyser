package com.larionov.enterprise.ui.web.controler.dto;

import com.larionov.enterprise.model.StockStatistic;

public class StockStatisticDto {
    //TODO make tdo
    private final StockStatistic stockStatistic;

    public StockStatisticDto(StockStatistic stockStatistic) {
        this.stockStatistic = stockStatistic;
    }

    public StockStatistic getStockStatistic() {
        return stockStatistic;
    }
}
