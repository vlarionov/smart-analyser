var STOCK_STATISTIC_ELEMENT_ID = "#stockStatistic";

$(function () {
    showStockStatistic();
});

function showStockStatistic() {
    function showStockStatisticData(data) {
        var priceQuoteList = data.stockStatistic.priceQuoteList;
        var minTime = priceQuoteList[0].instant.epochSecond;

        var pointArray = [];
        for (var i = 0; i < priceQuoteList.length; i++) {
            x = priceQuoteList[i].instant.epochSecond - minTime;
            y = priceQuoteList[i].price;
            pointArray[i] = [x, y];
        }

        $("#stockStatisticDiagram").sparkline(
            pointArray,
            {
                width: 512,
                height: 256
            }
        );
    }

    $.ajax({
        type: "GET",
        url: "data/get",
        success: showStockStatisticData,
        async: true
    });
}