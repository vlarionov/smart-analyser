package com.larionov.analysis.data;

public class Balance {
    private final double cashBalance;
    private final double assetBalance;

    public Balance(double cashBalance, double assetBalance) {
        this.cashBalance = cashBalance;
        this.assetBalance = assetBalance;
    }

    public double getCashBalance() {
        return cashBalance;
    }

    public double getAssetBalance() {
        return assetBalance;
    }
}
