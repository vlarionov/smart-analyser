package com.larionov.analysis.data;

public class TimePoint extends TimeEntity {
    private final double value;

    public TimePoint(int time, double value) {
        super(time);
        this.value = value;
    }

    public TimePoint(int time) {
        this(time, 0d);
    }

    public double getValue() {
        return value;
    }
}
