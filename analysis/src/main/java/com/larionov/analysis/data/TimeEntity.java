package com.larionov.analysis.data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class TimeEntity implements Comparable<TimeEntity> {
    @Min(0)
    private final int time;

    public TimeEntity(int time) {
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    @Override
    public final int compareTo(@NotNull TimeEntity otherTimeEntity) {
        return time - otherTimeEntity.getTime();
    }
}
