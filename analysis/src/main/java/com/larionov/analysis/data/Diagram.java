package com.larionov.analysis.data;

import com.larionov.analysis.data.validation.OrderedList;

import javax.validation.constraints.NotNull;
import java.util.List;

public class Diagram<T extends TimeEntity> {
    @NotNull
    @OrderedList
    private final List<T> entitySequence;

    public Diagram(List<T> entitySequence) {
        this.entitySequence = entitySequence;
    }

    public List<T> getEntitySequence() {
        return entitySequence;
    }

}
