package com.larionov.analysis.data.validation.validators;

import com.larionov.analysis.data.TimeEntity;
import com.larionov.analysis.data.validation.OrderedList;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.security.InvalidParameterException;
import java.util.Iterator;
import java.util.List;

public class OrderedListValidator implements ConstraintValidator<OrderedList, Object> {
    @Override
    public void initialize(OrderedList constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) return true;

        try {
            List<?> list = (List) value;
            Iterator<?> iterator = list.iterator();
            if (!list.isEmpty()) {
                TimeEntity previous = (TimeEntity) iterator.next();
                do {
                    TimeEntity current = (TimeEntity) iterator.next();
                    if (previous.compareTo(current) > 0) {
                        throw new InvalidParameterException();
                    }
                } while (iterator.hasNext());
            }
            return true;
        } catch (ClassCastException | InvalidParameterException e) {
            context.buildConstraintViolationWithTemplate("Not a ordered list");
            return false;
        }
    }

}
