package com.larionov.analysis.data;


import javax.validation.constraints.NotNull;

public class TimeAction extends TimeEntity {
    @NotNull
    private ActionType type;
    private double amount;

    public TimeAction(int time, ActionType type, double amount) {
        super(time);
        this.type = type;
        this.amount = amount;
    }

    public ActionType getType() {
        return type;
    }

    public double getAmount() {
        return amount;
    }

    public enum ActionType {
        PLUS(1), MINUS(-1);

        private final int multiplier;

        ActionType(int multiplier) {
            this.multiplier = multiplier;
        }

        public int getMultiplier() {
            return multiplier;
        }
    }


}
