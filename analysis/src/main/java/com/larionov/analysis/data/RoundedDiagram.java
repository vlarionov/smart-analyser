package com.larionov.analysis.data;

import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

public abstract class RoundedDiagram extends Diagram<TimePoint> {
    private final NavigableSet<TimePoint> navigableDiagram;

    public RoundedDiagram(List<TimePoint> pointSequence) {
        super(pointSequence);
        this.navigableDiagram = new TreeSet<>(pointSequence);
    }

    public NavigableSet<TimePoint> getNavigableDiagram() {
        return navigableDiagram;
    }

    public abstract TimePoint getRoundedPoint(int time);

}
