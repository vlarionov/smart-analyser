package com.larionov.analysis.data;

import javax.annotation.Nullable;
import java.util.List;
import java.util.NavigableSet;

public class LineRoundedDiagram extends RoundedDiagram {

    public LineRoundedDiagram(List<TimePoint> pointSequence) {
        super(pointSequence);
    }

    @Override
    public TimePoint getRoundedPoint(int time) {
        TimePoint currentPoint = new TimePoint(time);
        TimePoint earlierPoint = getNavigableDiagram().lower(currentPoint);
        TimePoint laterPoint = getNavigableDiagram().higher(currentPoint);
        return linearRound(time, earlierPoint, laterPoint);
    }

    private TimePoint linearRound(int time, @Nullable TimePoint earlierPoint, @Nullable TimePoint laterPoint) {
        if (earlierPoint == null) {
            return laterPoint;
        }
        if (laterPoint == null) {
            return earlierPoint;
        }
        // x(t) = x0 + t * k, k = (x1 - x0)/(t1 - t0)
        double k = (laterPoint.getValue() - earlierPoint.getValue()) / laterPoint.getTime() - earlierPoint.getTime();

        double roundedValue = earlierPoint.getValue() + (time - earlierPoint.getTime()) * k;

        return new TimePoint(time, roundedValue);
    }

}
