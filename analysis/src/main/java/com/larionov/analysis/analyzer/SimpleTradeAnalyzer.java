package com.larionov.analysis.analyzer;

import com.larionov.analysis.data.*;

import java.util.ArrayList;
import java.util.List;

public class SimpleTradeAnalyzer implements TradeAnalyzer {

    @Override
    public Diagram<TimeAction> getTradeStrategy(RoundedDiagram pointDiagram, Balance balance) {
        List<TimeAction> result = new ArrayList<>();
        List<TimePoint> pointList = pointDiagram.getEntitySequence();
        TimePoint previousPoint = pointList.get(0);
        for (int i = 1; i < pointList.size(); i++) {
            TimePoint currentPoint = pointList.get(i);
            if (currentPoint.getValue() > previousPoint.getValue()) {
                result.add(new TimeAction(
                        currentPoint.getTime(),
                        TimeAction.ActionType.PLUS,
                        balance.getCashBalance()
                ));
            } else {
                result.add(new TimeAction(
                        currentPoint.getTime(),
                        TimeAction.ActionType.MINUS,
                        currentPoint.getValue() * balance.getAssetBalance()
                ));
            }
            previousPoint = currentPoint;
        }
        return new Diagram<>(result);
    }
}
