package com.larionov.analysis.analyzer;

import com.larionov.analysis.data.Diagram;
import com.larionov.analysis.data.Balance;
import com.larionov.analysis.data.RoundedDiagram;
import com.larionov.analysis.data.TimeAction;

public interface RetrospectiveAnalyser {

    Balance analyze(RoundedDiagram pointDiagram, Diagram<TimeAction> actionDiagram);

}
