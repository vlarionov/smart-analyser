package com.larionov.analysis.analyzer;

import com.larionov.analysis.data.*;

import java.util.List;

public class SimpleRetrospectiveAnalyser implements RetrospectiveAnalyser {

    @Override
    public Balance analyze(RoundedDiagram pointDiagram, Diagram<TimeAction> actionDiagram) {
        double profit = 0.0;
        double stockBalance = 0.0;

        for (TimeAction action : actionDiagram.getEntitySequence()) {
            TimePoint point = pointDiagram.getRoundedPoint(action.getTime());
            profit += action.getType().getMultiplier() * action.getAmount();
            stockBalance -= point.getValue() / (action.getAmount());
        }
        return new Balance(profit, stockBalance);
    }
}
