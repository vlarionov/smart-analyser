package com.larionov.analysis.analyzer;

import com.larionov.analysis.data.Balance;

public class AnalysisResult {
    private final Balance balance;

    public AnalysisResult(Balance balance) {
        this.balance = balance;
    }

    public Balance getBalance() {
        return balance;
    }
}
